package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"slices"
	"strings"
)

func verifyHandler(w http.ResponseWriter, r *http.Request) {
	clientCertValue := r.Header.Get("X-Forwarded-Tls-Client-Cert-Info")
	allowedCertsValue := r.Header.Get("X-Forwarded-Allowed-Subjects")
	clientCertDecoded, err := url.QueryUnescape(clientCertValue)
	if err != nil {
		log.Fatal(err)
		http.Error(w, "Error processing request.", http.StatusInternalServerError)
		return
	}
	params, err := url.ParseQuery(clientCertDecoded)
	if err != nil {
		log.Fatal(err)
		http.Error(w, "Error processing request.", http.StatusInternalServerError)
		return
	}
	clientCertSubject := strings.TrimRight(strings.TrimLeft(strings.Replace(params.Get("Subject"), "CN=", "", 1), "\""), "\"")
	allowedCertSubjects := strings.Split(allowedCertsValue, ",")
	if slices.Contains(allowedCertSubjects, clientCertSubject) {
		fmt.Fprintf(w, "ok")
	} else {
		http.Error(w, "Client cert permission denied.", http.StatusUnauthorized)
		return
	}
}

func main() {
	http.HandleFunc("/verify", verifyHandler)

	fmt.Printf("Starting server at port 8071\n")
	if err := http.ListenAndServe(":8071", nil); err != nil {
		log.Fatal(err)
	}
}
